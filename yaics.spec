%define version 0.6-1
%define release 1
%define name yaics
%define reponame yaics

Summary: GNU Social client
Name: %{name}
Version: 0.6
Release: 1
License: GPLv3+
Prefix: /usr/bin
URL: https://gitlab.com/stigatle/yaics/
Source0: https://gitlab.com/stigatle/yaics/-/archive/0.6-1/yaics-0.6-1.tar.gz
BuildRequires: aspell-devel
BuildRequires: qt5-devel
BuildRequires: qt5-qtwebkit-devel

Requires: aspell
Requires: qt5
Requires: qt5-qtwebkit

%description
Yaics is a simple Qt-based desktop client for GNU social, a
distributed social networking system.

%global debug_package %{nil}

%prep
%setup -q -n %{name}-0.6-1

%build
qmake-qt5 CONFIG+=%{_arch} yaics.pro PREFIX=$RPM_BUILD_ROOT%{_prefix}
make

%install
rm -rf ${buildroot}
mkdir %{buildroot}%{prefix} -p
%make_install

%clean
rm -rf ${buildroot}

%files
%license COPYING
%doc AUTHORS ChangeLog README.md
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/*.png
%dir %{_datadir}/appdata/
%{_datadir}/appdata/%{name}.appdata.xml


%changelog
* Sat Jul 1 2017 Stig Atle Steffensen  <StigAtle@cryptolab.net> - 0.6
- Fixed status icon crash when exiting application.
- Fixed position of attached images in statuses.
- Aligned reply\fav\ etc buttons to the left side.
- Status window now stores its size.
- Removed ',' when you have prepend with nicks in replies.
- Fixed flickering GUI when timeline is filled with statuses.


