/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "statuswindow.h"
#include "util/yaicssettings.h"
#include <QDebug>
//------------------------------------------------------------------------------

void StatusWindow::closeEvent(QCloseEvent *) {
 YaicsSettings* settings = YaicsSettings::getSettings();
 settings->setStatusWindowGeometry(this->saveGeometry());
 qDebug() << "statuswindow: close event triggered, saving window state";
}

StatusWindow::StatusWindow(QSocialAPI* sa, const QString& reply_to_user,
                           message_id_t id, QWidget* parent) :
  MessageWindow(sa, reply_to_user, parent), in_reply_to_id(id)
{
    YaicsSettings* settings = YaicsSettings::getSettings();
    this->restoreGeometry(settings->getStatusWindowGeometry());

  sendButton->setText(tr("Send notice"));
  if (in_reply_to_id == -1)
    infoLabel->setText(tr("Write a new notice"));
  else
  {
    infoLabel->setText(tr("Write a reply to %1").arg(reply_to_user));
    QStringList list;
    list = sa->getStatus(in_reply_to_id)->getPlainText().split(' ');
    QString replyToString = "";

    for (int c = 0; c < list.count(); ++c)
    {
        if (list.at(c).startsWith("@"))
        {
            //qDebug() << "Line" << c+1 << "=" << list.at(c);
            //Filter out your own nick from replies..
            if (!list.at(c).contains(sa->getUsername()))
            {
                replyToString.append(list.at(c) + " ");
            }
        }
    }

    textEdit->setText(replyToString);
    QTextCursor tc = textEdit->textCursor();
    tc.setPosition(replyToString.count());
    textEdit->setTextCursor(tc);
  }
}

//------------------------------------------------------------------------------

void StatusWindow::onStatusReady(StatusMessage*) {
  done(QDialog::Accepted);
  emit messageSent();
}

//------------------------------------------------------------------------------

QSocialAPIRequest* StatusWindow::sendMessageRequest(const QString& msg) {
  QSocialAPIRequest* rq = socialAPI->postUpdate(msg, in_reply_to_id);
  connect(rq, SIGNAL(statusReady(StatusMessage*)),
          this, SLOT(onStatusReady(StatusMessage*)));
  return rq;
}
