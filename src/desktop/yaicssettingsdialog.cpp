/*
  Copyright 2010-2015 Mats Sjberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "yaicssettingsdialog.h"

#include "util/util.h"
#include <QColorDialog>
#include <QNetworkProxy>
#include <QStyleFactory>
#include <QMessageBox>
#include <qfont.h>
#include <qfontdialog.h>
//------------------------------------------------------------------------------

YaicsSettingsDialog::YaicsSettingsDialog(YaicsSettings* settings,
                                         QWidget* parent) :
  QDialog(parent),
  s(settings)
{
  setupUi(this);
  qtStyleComboBox->addItems(QStyleFactory::keys());
}

//------------------------------------------------------------------------------
void YaicsSettingsDialog::on_buttonBox_accepted() {
  s->setReloadTime(updateTimeSpinBox->value());
  s->setUseTrayIcon(trayIconCheckBox->isChecked());
  s->setTextToFilter(textToFilterEdit->text());
  s->setshowAttachment(checkBox_showAttachments->isChecked());
  // useApiRepeat = apiRepeatCheckBox->isChecked();
  s->setPrependReplies(prependRepliesCheckBox->isChecked());

  s->setHomeNotify((YaicsSettings::notifyType)
                   homeNotifyComboBox->currentIndex());
  s->setMentionsNotify((YaicsSettings::notifyType)
                       mentionsNotifyComboBox->currentIndex());

  s->setAPIUrl(slashify(APIUrlEdit->text()));
  s->setUsername(usernameEdit->text());
  s->setPassword(passwordEdit->text());
  s->setIgnoreSSLWarnings(secureWarningCheckBox->isChecked());
  s->setQtStyle(qtStyleComboBox->currentText());
  s->setEnableFancyUrl(checkBox_enableFancyUrl->isChecked());

  if (maxNumStatusesSpinBox->value() > 500)
  {
    s->setMaxNumStatuses(maxNumStatusesSpinBox->value());
  }else
  {
      s->setMaxNumStatuses(500);
  }
  // Set proxy params.
  if (proxyTypeComboBox->currentText() != "None") {
    QNetworkProxy proxy;
    if (proxyTypeComboBox->currentText() == "Socks5") {
      proxy.setType(QNetworkProxy::Socks5Proxy);
    }
    if (proxyTypeComboBox->currentText() == "HTTP")
      proxy.setType(QNetworkProxy::HttpProxy);

    proxy.setHostName(proxyIPEdit->text());
    proxy.setPort(proxyPortEdit->text().toInt());
    proxy.setUser(proxyUsernameEdit->text());
    proxy.setPassword(proxyPasswordEdit->text());
    QNetworkProxy::setApplicationProxy(proxy);
  } else {
    // Clear the proxy.
    QNetworkProxy::setApplicationProxy(QNetworkProxy::NoProxy);
  }
  s->setProxyType(proxyTypeComboBox->currentIndex());
  s->setProxyIP(proxyIPEdit->text());
  s->setProxyPort(proxyPortEdit->text());
  s->setProxyUsername(proxyUsernameEdit->text());
  s->setProxyPassword(proxyPasswordEdit->text());
  s->setEnablePublicTimeline(publicTimelineCheckBox->isChecked());

  emit settingsChanged();
    QMessageBox::information(NULL,"Information",tr("Yaics restart is mandatory for settings to take effect.\nPlease restart Yaics."),"Ok");
}

//------------------------------------------------------------------------------

void YaicsSettingsDialog::setVisible(bool visible) {
  if (visible)
    updateUI();
  QDialog::setVisible(visible);
}

//------------------------------------------------------------------------------

void YaicsSettingsDialog::updateUI() {
  updateTimeSpinBox->setValue(s->getReloadTime());
  trayIconCheckBox->setChecked(s->getUseTrayIcon());
  textToFilterEdit->setText(s->getTextToFilter());
  //  apiRepeatCheckBox->setChecked(useApiRepeat);
  prependRepliesCheckBox->setChecked(s->getPrependReplies());
  
  homeNotifyComboBox->setCurrentIndex(s->getHomeNotify());
  mentionsNotifyComboBox->setCurrentIndex(s->getMentionsNotify());

  if (s->getMaxNumStatuses() > 500)
  {
    maxNumStatusesSpinBox->setValue(s->getMaxNumStatuses());
  }else
  {
      maxNumStatusesSpinBox->setValue(500);
  }

  APIUrlEdit->setText(s->getAPIUrl());
  usernameEdit->setText(s->getUsername());
  passwordEdit->setText(s->getPassword());
  secureWarningCheckBox->setChecked(s->getIgnoreSSLWarnings());
    checkBox_enableFancyUrl->setChecked(s->getEnableFancyUrl());
  if (qtStyleComboBox->findText(s->getQtStyle()) != -1)
    qtStyleComboBox->setCurrentIndex(qtStyleComboBox->findText(s->getQtStyle()));
  else
    qtStyleComboBox->setCurrentIndex(0);

  publicTimelineCheckBox->setChecked(s->getEnablePublicTimeline());
  checkBox_showAttachments->setChecked(s->getshowAttachment());
  proxyTypeComboBox->setCurrentIndex(s->getProxyType());
  proxyUsernameEdit->setText(s->getProxyUsername());
  proxyPasswordEdit->setText(s->getProxyPassword());
  proxyIPEdit->setText(s->getProxyIP());
  proxyPortEdit->setText(s->getProxyPort());

  if (s->getUsername().isEmpty())
    tabWidget->setCurrentIndex(1);
}

void YaicsSettingsDialog::on_setLinkColourButton_clicked() {
  QColor colour = QColorDialog::getColor(s->getlinkColour(), this);
  if (colour.isValid()) {
    QString textColourName = colour.name();
    s->setlinkColour(textColourName);
    //QMessageBox::information(this, "Text Colour", "You selected "+textColourName);
  }
}

void YaicsSettingsDialog::on_buttonBox_rejected() {
  QDialog::setVisible(false);
}

void YaicsSettingsDialog::on_pushButton_clicked()
{
    bool ok;
    QFont font = QFontDialog::getFont(
                    &ok, QFont(s->getfontName(), s->getfontSize()), this);
    if (ok) {
        // the user clicked OK and font is set to the font the user selected
        QString fontFamily = font.family();
        int fontSize = font.pointSize();
        s->setfontName(fontFamily);
        s->setfontSize(fontSize);
        s->setfontItalic(font.italic());
        s->setfontBold(font.bold());
    } else {
        // the user canceled the dialog; font is set to the initial
        // value, in this case Helvetica [Cronyx], 10
    }
}
